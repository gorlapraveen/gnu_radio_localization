/* Generated automatically. */
static const char configuration_arguments[] = "../gcc-8.2.0-source/gcc-8.2.0/configure --enable-languages=c,c++,fortran --enable-checking=release --disable-libstdcxx-pch --enable-libgomp --enable-lto --enable-gold --with-plugin-ld=gold --prefix=/usr/local/gcc-8.2.0";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
