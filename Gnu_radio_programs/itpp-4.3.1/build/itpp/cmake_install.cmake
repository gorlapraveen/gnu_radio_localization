# Install script for directory: /home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/itpp

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libitpp.so.8.2.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libitpp.so.8"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libitpp.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/build/itpp/libitpp.so.8.2.1"
    "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/build/itpp/libitpp.so.8"
    "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/build/itpp/libitpp.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libitpp.so.8.2.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libitpp.so.8"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libitpp.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/itpp" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/itpp" TYPE FILE FILES "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/build/itpp/config.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/itpp" TYPE FILE FILES "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/build/itpp/itexports.h")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/itpp" TYPE FILE FILES
    "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/extras/itsave.m"
    "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/extras/itload.m"
    "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/extras/pyitpp.py"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/doc/itpp" TYPE DIRECTORY FILES "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/build/html")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE FILE PERMISSIONS OWNER_READ OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE FILES "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/build/itpp-config")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/man/man1" TYPE FILE FILES "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/build/itpp-config.1")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/praveen-ubuntu-radio/Documents/Gnu_radio/itpp-4.3.1/build/itpp.pc")
endif()

